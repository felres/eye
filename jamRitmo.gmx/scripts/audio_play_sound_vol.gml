/// audio_play_sound_vol(soundid, priority, loops, vol)

var temp = audio_play_sound(argument0, argument1, argument2);
audio_sound_gain(temp, argument3, 0);

return temp;
