/// body_percentage_inside_water(waterY)

var water_level = argument0;
var height = abs(bbox_top - bbox_bottom);

if (bbox_top >= water_level)
    return 1;
else if (bbox_bottom <= water_level)
    return 0;
else
    return abs(bbox_bottom-water_level)/height;
