/// draw_laser_line(x1, y1, x2, y2, width, outline width, segments, variation, col1, col2, circle width proportion)

var x1, y1, x2, y2, w, ow, segments, v, col1, col2;
x1 = argument0;
y1 = argument1;
x2 = argument2;
y2 = argument3;
w = argument4;
ow = argument5;
segments = argument6;
v = argument7;
col1 = argument8;
col2 = argument9;
circ = argument10;

for(var i = 0; i < segments; i++)
{
    var col = merge_colour(col1, col2, i/segments);
    draw_set_colour( col );
    var f_outline = lerp(w + ow, w,i/segments);
    
    draw_circle(x1, y1, gauss(f_outline*circ, v), false);
    draw_line_width(x1, y1, x2, y2, gauss(f_outline, v) );
}
draw_set_colour(col2);
draw_circle(x1, y1, gauss(w*circ, v), false);
draw_line_width(x1, y1, x2, y2, gauss(w, v));


draw_set_color(c_white);
