/// fish_draw()

// Draw pie
if draw_debug
{
    var ang1 = direction - fov_angle/2;
    var ang2 = direction + fov_angle/2;
    draw_set_alpha(0.2);
    draw_set_colour(c_white);
    draw_pie(x-sense, y-sense,
                x+sense, y+sense,
                x+lengthdir_x(sense, ang1), y+lengthdir_y(sense, ang1),
                x+lengthdir_x(sense, ang2), y+lengthdir_y(sense, ang2),
                false, 0);
    draw_set_alpha(1);
}

//draw_pie_easy(x, y, lifespan, lifespan_start, c_green, sense, 0.4);
//draw_pie_easy(x, y, hunger, 1, c_maroon, sense/2, 0.4);
// draw self
image_angle = direction;
if hunger==1
    image_alpha = 0.5;
var minscale = max(image_xscale, min_draw_scale)
draw_sprite_ext(sprite_index, image_index, x, y, minscale, minscale, image_angle, image_blend, image_alpha);
image_alpha = 1;


// draw fishe inside fov
if draw_debug
{
    var c = merge_colour(c_white, c_red, angle_difference(unoccupied_angle, direction)/180);
    draw_line_width_colour(x, y, x+lengthdir_x(sense, unoccupied_angle), y+lengthdir_y(sense, unoccupied_angle), 1, c, c);
    if( ds_exists(fov_list, ds_type_list) )
    {
        for(var i = 0; i < ds_list_size(fov_list); i++)
        {
            var inst = ds_list_find_value(fov_list, i);
            if instance_exists(inst)
            {
                var w = 2;
                var c = merge_colour(c_red, c_white, point_distance(x, y, inst.x, inst.y)/sense);
                draw_line_width_colour(x, y, inst.x, inst.y, w, c, c);
            }
        }
    }
    
    draw_text(x, y, string(hunger));
}
