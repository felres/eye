/// fish_point_towards_mouse
if (mouse_check_button(mb_left))
{
    var arg0, arg1, arg2;
    arg0 = -point_direction(x, y, mouse_x, mouse_y);
    arg1 = turnrate;
    arg2 = 0;
    phy_rotation += median(-arg1, arg1, (1-arg2) * angle_difference(arg0, phy_rotation));
}
