///fish_set_properties()
// state 0: flock movement, 1: search place to lay egg
team = choose("male","female");
fov_angle = 300;
lifespan_start = room_speed * 20;
lifespan = lifespan_start;
sexual_maturity = 0.3; // punto del lifespan en el que ya se puede reproducir
hunger = 0.4; //starting hunger
hunger_restore = 0.39;
hunger_increment = 0//0.0020;
lifespan_step_reduce = 0;
direction = random(360);
set_scale(0);
precisecollisions = true;
immortal = true;

// stats that make sense that depend on age
matured_speed = 5;
matured_random_distribution = 1; random_distribution = 0;
matured_sense = 30; sense = 0;
matured_turnrate = 20; turnrate = 0;
hungry_turnrate_multiplier = 1.7; //3
obstacle_turnrate_multiplier = 2; //1.8

// drawing
fov_list = noone; 
draw_debug = false;
unoccupied_angle = direction;
min_draw_scale = 0.2;
