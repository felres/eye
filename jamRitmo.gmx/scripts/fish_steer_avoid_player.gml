/// player
var player_sense_multiplier = 1;
var player = collision_fov(x, y, objPlayer, sense*player_sense_multiplier, direction, fov_angle, true);
if(player)
{
    var turnrate_player = turnrate * 2;
    var tdir = point_direction(player.x, player.y, x, y);
    var proximity = point_distance(x, y, player.x, player.y)/(sense*player_sense_multiplier);
    ease_towards_direction(tdir, turnrate_player, 0);
    
    while instance_place(x, y, objPlayer)
    {
        x += lengthdir_x(max(1,speed), tdir);
        y += lengthdir_y(max(1,speed), tdir);
    }
}

