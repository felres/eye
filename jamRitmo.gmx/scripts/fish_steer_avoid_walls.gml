/// wall "collisions"
unoccupied_angle = direction;
var extra_force = 1;
for(var i = 0; i < 360; i++)
{
    var s = 1; if is_even(i) s = -1;
    var ang = direction + i*s;
    var x2 = x+lengthdir_x(sense, ang);
    var y2 = y+lengthdir_y(sense, ang);
    var wall = collision_line(x, y, x2, y2, objFloatableParent, precisecollisions, true);
    if !wall
    {
        unoccupied_angle = ang;
        break;
    }
    else
        extra_force = max(1, abs(wall.speed));
}
var wall_turnrate = extra_force*turnrate*obstacle_turnrate_multiplier;
ease_towards_direction(unoccupied_angle, wall_turnrate, 0);

/// if close to surface, swim downwards
var mindist = 80;
if( instance_exists(objOceanManager) && (point_distance(x, y, x, objOceanManager.y) < mindist) )
{
    var oceanY = objOceanManager.y;
    var proximity_to_surface = 0.5 * lerp(0, 1, (room_height-oceanY)/(room_height-y) );
    ease_towards_direction(270, turnrate, 1-proximity_to_surface);
}

var sea_depth = room_height;
/// if close to sea_depth, swim upwards
if((point_distance(x, y, x, sea_depth)<mindist))
{
    ease_towards_direction(90, turnrate, 0.5);
}
