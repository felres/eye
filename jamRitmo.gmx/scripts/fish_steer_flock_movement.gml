/// fish_steer_movement()
var dir_sum = 0;
var x_sum = 0;
var y_sum = 0;
if( ds_exists(fov_list, ds_type_list) )
{
    var length = ds_list_size(fov_list);
    /// Separation
    // Have each unit steer to avoid hitting its neighbors.
    for(var i = 0; i < length; i++)
    {
        var inst = ds_list_find_value(fov_list, i);
        var easing = distance_to_object(inst)/sense;
        ease_towards_direction(point_direction(inst.x, inst.y, x, y), turnrate, easing);
        
        // for alignment and cohesion
        dir_sum += inst.direction;
        x_sum += inst.x;
        y_sum += inst.y;
    }
    
    /// Cohesion
    // Have each unit steer toward the average position of its neighbors.
    var average_neighbor_dir = point_direction(x, y, x_sum/length, y_sum/length);
    //entre mas lejos el punto, mas fuerte
    var easing = 1 - point_distance(x, y, x_sum/length, y_sum/length)/sense; 
    ease_towards_direction(average_neighbor_dir, turnrate, easing);
    
    /// Alignment
    // Have each unit steer so as to align itself to the average heading of its neighbors.
    ease_towards_direction(dir_sum/length, turnrate, easing);
}
