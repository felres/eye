/// fish_steer_search_nest()

image_blend = c_teal;


if place_meeting(x, y, objWall) || (lifespan-(5) <= 0)
{
    canLayEgg = false;
    repeat(irandom_range(2, 4)) instance_create(x, y, objEgg);
    particle_appear(x, y);
    state = 0;
    image_blend = c_white;
}

if !is_eve
    ease_towards_direction(point_direction(x, y, xstart, ystart), turnrate, random_range(0.9, 1));
    
var nest = collision_fov(x, y, objWall, sense*2, direction, fov_angle, true);
if(nest)
{
    ease_towards_direction(point_direction(x, y, nest.x, nest.y), turnrate, 0);
}
