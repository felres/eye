var food = collision_fov(x, y, objPlankton, sense, direction, fov_angle, true);
if(food)
{
    var hungry_force = max(hunger*turnrate*hungry_turnrate_multiplier, 0);
    ease_towards_direction(point_direction(x, y, food.x, food.y), hungry_force, 0);
}
