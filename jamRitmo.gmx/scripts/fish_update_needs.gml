/// fish_update_needs()

var food = instance_place(x, y, objPlankton);
if food && (hunger>-1)
{
    instance_destroy(food);
    hunger-=hunger_restore;
}
hunger = min(hunger+hunger_increment, 1)
    
/// grow up
var maturity_point = lifespan_start*(1-sexual_maturity);
var scale = 1-(lifespan-maturity_point)/(lifespan_start-maturity_point);
var scale = clamp(scale, 0, 1);
set_scale( scale )
// adjust stats to age
speed = matured_speed*scale;
random_distribution = matured_random_distribution*scale;
sense = matured_sense*scale;
turnrate = matured_turnrate + (matured_turnrate*(1-scale));

/// give birth
var maturity_reached = lifespan < maturity_point;
if( maturity_reached && (hunger<0)  )
{
    hunger += 1;
    var i = 6;
    var xx = x+random_range(-i, i);
    var yy = y+random_range(-i, i);
    var eggs_to_make = irandom_range(3, 4);
    repeat(eggs_to_make) instance_create(xx, yy, objEgg);
    particle_appear(x, y);
}
    
// die eventually
var starve_amount = 0;
if hunger>-0.5
    starve_amount = hunger;
lifespan -= lifespan_step_reduce + starve_amount;
if (lifespan <= 0) && !immortal
    instance_destroy();
