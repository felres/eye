/// fish_update_neighbor_data()
if( ds_exists(fov_list, ds_type_list) )
{
    ds_list_destroy(fov_list);
    fov_list = noone;
}
fov_list = objects_in_fov(x, y, objFish, sense, direction, fov_angle, true);
