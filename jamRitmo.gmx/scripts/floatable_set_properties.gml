def_gravity = 0.8;
air_friction = 0.4;
water_friction = 0.58;
displacementMultiplierDefault = 2.1;
displacementMultiplier = displacementMultiplierDefault;
mass = 1;
bounce = 1.2;
