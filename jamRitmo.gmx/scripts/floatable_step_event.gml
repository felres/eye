// if submerged
var waterlevel = objOceanManager.y;
if( bbox_bottom > waterlevel )
{
    buoyancy = body_percentage_inside_water(waterlevel) * def_gravity * displacementMultiplier;
    gravity = def_gravity - buoyancy;
    friction = lerp(air_friction, water_friction, body_percentage_inside_water(waterlevel));
}
else
{
    friction = air_friction;
    gravity = def_gravity;
}

hcol = instance_place(x + hspeed, y, objFloatableParent)
if( hcol )
{
    var mass1 = sprite_width*sprite_height * mass;
    var mass2 = hcol.sprite_width*hcol.sprite_height * hcol.mass;
    var mass_sum = mass1 + mass2;
    var h_apply = hspeed;
    with hcol
        hspeed = h_apply * mass1/(mass_sum) * bounce;
    hspeed = -h_apply * mass2/(mass_sum) * bounce;
}

vcol = instance_place(x, y + vspeed, objFloatableParent)
if( vcol )
{
    var mass1 = sprite_width*sprite_height * mass;
    var mass2 = vcol.sprite_width*vcol.sprite_height * vcol.mass;
    var mass_sum = mass1 + mass2;
    var v_apply = vspeed;
    with vcol
        vspeed = v_apply * mass1/(mass_sum) * bounce;
    vspeed = -v_apply * mass2/(mass_sum) * bounce;
}
