/// grade_beat(total steps, beat step num)

var total_steps = argument0;
var beat_n = argument1;
var a = choose(3, 3, 3, 4);

if beat_n == -1
    return -1;

// perfect beat
if (!is_even(total_steps) && (abs(beat_n-ceil(total_steps/2)) <= a) ) ||
    ( is_even(total_steps) && (abs(beat_n-ceil(total_steps/2)) <= a) )
return 5;

// low but ok
return 2;
