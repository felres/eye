/// liquid_column_corners(i)

var i = argument0;

x1 = bodyX + (i * columnWidth);
y1 = bodyY + targetHeight[i] - height[i];
x2 = x1 + columnWidth;
y2 = bodyY + targetHeight[i];
right_y1 = y1;

if(i < columns-1)
    right_y1 = bodyY + targetHeight[i+1] - height[i+1];
