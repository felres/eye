/// phy_ease_towards_direction(target,rate,easing)
//
//  Rotates the calling instance towards the target direction,
//  at a given rate and easing. The larger the easing factor,
//  the more gradually the turn completes.
//
//      target      angle to turn towards (degrees), real
//      rate        maximum turning rate (degrees), real
//      easing      amount of easing [0..1]
//                  where 0 is no easing and 1 is no turning at all, real
//
/// GMLscripts.com/license
{
    var arg0, arg1, arg2;
    arg0 = -argument0;
    arg1 = argument1;
    arg2 = argument2;
    phy_rotation += median(-arg1, arg1, (1-arg2) * angle_difference(arg0, phy_rotation));
    return 0;
}
